/*
 * Első nekifutás: formális átírás (kommentezve este lesz a blogon) 2012. okt.
 * 2. Bátfai Norbert
 */

public class ExorToro {

  // Néhány konstans (ezek az átírandó C kód örökségei). 
  /**
   * Max. mekkora lehet a kezelt titkosított rész.
   */
  public static final int MAX_TITKOS = 65535;
  /**
   * Bufferelten olvasunk, (ez az átírandó C kód öröksége).
   */
  public static final int OLVASAS_BUFFER = 256;
  /**
   * Dupla PIN méretű a kulcs.
   */
  public static final int KULCS_MERET = 8;
  /*
   * Ahogy C-ben is, most is bájttömbökkel dolgozunk alapvetően, ez is
   * egyértelműen a C árírata.
   */
  /**
   * A kulcsot tárolja.
   */
  byte[] kulcs = new byte[KULCS_MERET];
  /**
   * A titkos szöveget tárolja.
   */
  byte[] titkos = new byte[MAX_TITKOS];
  /**
   * A titkosból előállított szöveget tárolja.
   */
  byte[] tiszta = new byte[MAX_TITKOS];
  /*
   * Bár a Java tömb már tudja a saját méretét, de most a titkos buffer nyilván
   * nagyobb, ezért tároljuk ebben, hogy mennyi is valójában a beolvasott titkos
   * szöveg mérete.
   */
  int titkosMeret;
  /*
   * Hová írjuk majd az eredményt? Erre a csatornára. (Ez a Javát tanítokos
   * példa öröksége.)
   */
  java.io.OutputStream kimenőCsatorna;

  /** Az <code>ExorToro</code> objektum elkészítése. */
  public ExorToro(java.io.InputStream bejövőCsatorna,
          java.io.OutputStream kimenőCsatorna)
          /*
           * Az IO problémás dolog, ezzel jelezzük a hívónak, hogy ilyen
           * kivételt dobhatunk az ExotToro létrehozásakor, mert a kivétellel a
           * konstruktorban nem foglalkozunk, azok értelmes kezelését a hívóra
           * bízzuk.
           */
          throws java.io.IOException {

    // Beállítjuk, hová írjuk majd az eredményt
    this.kimenőCsatorna = kimenőCsatorna;
    // Ebbe a bufferbe olvasunk majd a bejövő csatornáról.
    byte[] buffer = new byte[OLVASAS_BUFFER];
    // Mennyit olvasunk a bejövő csatornáról?
    int olvasottBajtok;
    // Hol tart a bufferelt olvasás?
    int titkosIndex = 0;

    // titkos fajt berantasa, a C mintájára, de lényegesen egyszerűsítve, hiszen nincs mutató léptetés
    while ((olvasottBajtok = bejövőCsatorna.read(buffer, 0, OLVASAS_BUFFER)) != -1) {

      // System.arraycopy: olvas el az API doksit!
      if (titkosIndex + olvasottBajtok < MAX_TITKOS) {
        System.arraycopy(buffer, 0, titkos, titkosIndex, olvasottBajtok);
        // Hol tart a bufferelt olvasás?
        titkosIndex += olvasottBajtok;
      } else {
        System.arraycopy(buffer, 0, titkos, titkosIndex, MAX_TITKOS - titkosIndex);
        // Hol tart a bufferelt olvasás?
        titkosIndex += (MAX_TITKOS - titkosIndex);
        break;
      }
    }

    // Mennyi titkos szöveget dolgozunk fel:
    titkosMeret = titkosIndex;

    // A C kódban az str-es függvények helyes működéséhez kellett a \0
    // ennek öröksége csal ez a ciklus
    for (int i = 0; i < MAX_TITKOS - titkosIndex; ++i) {
      tiszta[titkosIndex + i] = titkos[titkosIndex + i] = '\0';
    }

    // Így tesztelheted, hogy mit is olvastál be:
    // (érdemes olvasható szöveggel tesztelni a beolvasást)
    // System.out.println(new String(titkos));

  }

  public void tores() throws java.io.IOException {

    byte[] jegyek = new byte[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    // A C kódból átmósolt ciklus a fenti jegyekre átírva:
    // osszes kulcs eloallitasa
    for (int ii = 0; ii <= 9; ++ii) {
      for (int ji = 0; ji <= 9; ++ji) {
        for (int ki = 0; ki <= 9; ++ki) {
          for (int li = 0; li <= 9; ++li) {
            for (int mi = 0; mi <= 9; ++mi) {
              for (int ni = 0; ni <= 9; ++ni) {
                for (int oi = 0; oi <= 9; ++oi) {
                  for (int pi = 0; pi <= 9; ++pi) {
                    // végigzongorázzuk az összes kulcsot, ezért Brute Force jellegű a törés.
                    kulcs[0] = jegyek[ii];
                    kulcs[1] = jegyek[ji];
                    kulcs[2] = jegyek[ki];
                    kulcs[3] = jegyek[li];
                    kulcs[4] = jegyek[mi];
                    kulcs[5] = jegyek[ni];
                    kulcs[6] = jegyek[oi];
                    kulcs[7] = jegyek[pi];

                    if (exor()) {
                      kimenőCsatorna.write(kulcs, 0, KULCS_MERET);
                      kimenőCsatorna.write(tiszta, 0, titkosMeret);
                      // Csúnya itt, de gyorsan kíratom a time paranccsal futtatva
                      // a szükséges időt majd.
                      System.exit(0);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

  }

  /** Másolva a C kódból, a típuskényszerítés kellett (de különben le sem fordul). */
  public boolean exor() {

    int kulcs_index = 0;

    for (int i = 0; i < titkosMeret; ++i) {

      tiszta[i] = (byte) (titkos[i] ^ kulcs[kulcs_index]);
      kulcs_index = (kulcs_index + 1) % KULCS_MERET;

    }

    return tiszta_lehet();
  }

  // A C kódból:
  public boolean tiszta_lehet() {
    // a tiszta szoveg valszeg tartalmazza a gyakori magyar szavakat
    // illetve az átlagos szóhossz vizsgálatával csökkentjük a
    // potenciális töréseket

    double szohossz = atlagosSzohossz();

    
    String tisztaStr = new String(tiszta);

    return szohossz > 6.0 && szohossz < 9.0
            && (tisztaStr.indexOf("hogy") != -1)
            && (tisztaStr.indexOf("nem") != -1);
  }

  // A C kódból:
  public double atlagosSzohossz() {
    int sz = 0;
    for (int i = 0; i < titkosMeret; ++i) {
      if (tiszta[i] == ' ') {
        ++sz;
      }
    }

    if (sz > 0) {
      return (double) titkosMeret / (double) sz;
    } else {
      // egy szó sem volt benne: 
      return Double.MAX_VALUE;
    }
  }

  public static void main(String[] args) {

    try {
      
      // Elkészítjük a törő objektumot (amely most a sztenderd inputról olvassa a 
      // titkos szöveget és a sztenderd kimenetre írja az eredményt) majd meg is
      // kérjük, hogy hajtsa végre a törést:
      new ExorToro(System.in, System.out).tores();

    } catch (java.io.IOException e) {

      // Ha volt kivétel kiírjuk a hívási láncot (a lényeg, hogy nem nyeljük le :)
      e.printStackTrace();

    }

  }
  /*
   * Az itteni kód egy része a konstruktorba, másik része a tores() módszerbe
   * kerül majd. int main (void) {
   *
   * char kulcs[KULCS_MERET]; char titkos[MAX_TITKOS]; char *p = titkos; int
   * olvasott_bajtok;
   *
   * // titkos fajt berantasa while ((olvasott_bajtok = read (0, (void *) p, (p
   * - titkos + OLVASAS_BUFFER < MAX_TITKOS) ? OLVASAS_BUFFER : titkos +
   * MAX_TITKOS - p))) p += olvasott_bajtok;
   *
   * // maradek hely nullazasa a titkos bufferben for (int i = 0; i <
   * MAX_TITKOS - (p - titkos); ++i) titkos[p - titkos + i] = '\0';
   *
   *...*/
}
