#include <stdio.h>

int main()
{
    //egesz
    int egesz=5;

    //egesz mutató mutató
    int *mutato=&egesz;

    //egesz referenciája
    int &ref=egesz;

    //egeszek tömbje
    int tomb[5];

    //egeszek tomnkenek referenciaja
    int (&ref)[5]=tomb;

    //egeszre mutató mutatók tömbje
    int tomb1[5];

    //egeszre mutató mutatót visszaadó függvény
    int *f1();

    //egeszre mutató mutatót visszaadó függvényre mutató mutató
    int *(*f2)();

    //egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény
    int (*f3(int egesz)) (int a, int b);

    //függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre
    int (*(*f4)(int))(int, int);
    
    return 0;
}