> (define (fakt n)
        (if (= n 1)
                1
                (* n (fakt (- n 1) ))
        ))

(display (fakt 5))
fakt120#t