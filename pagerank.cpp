#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main() 
{
    double linkmatrix[4][4] = 
    {
        {0.0,0.0,1.0/3,0.0},
        {1.0,1.0/2.0,1.0/3.0,1.0},                         
        {0.0,1.0/2.0,0.0,0.0},
        {0.0,0.0,1.0/3.0,0.0}
    };
    double elozo[4] = {1.0/4.0,1.0/4.0,1.0/4.0,1.0/4.0};   
    double ujabb[4] = {0.0,0.0,0.0,0.0};                                                         
    for (int i=0;i<10;i++)
    {                                  
        for (int j=0; j<4; j++)
        {                            
            for (int k=0; k<4; k++)
            {                        
                if (k==0)
                {
		    ujabb[j] = 0.0;
		}                 
                if (linkmatrix[j][k] !=0.0) 
		        {               
                    ujabb[j] = ujabb[j]+(elozo[k]*linkmatrix[j][k]);      
                }
            }
        }
        for (int l=0;l<4;l++)
    	{
    		elozo[l]=ujabb[l];
    	}               
        for (int l=0; l<4;l++)
    	{
    	cout << ujabb[l] <<'\n';  
                if (l==3)
    	        {
    		        cout << "-------------------------------\n";
    	        }
    	} 
    }
    return 0;
}
